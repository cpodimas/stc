import React from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import {Form} from "react-bootstrap";


import Languages from './languages.json';


/*
var Languages={
    "russian":"русский",
    "english":"английский",
    "turkish":"турецкий"
};
*/


export default class Translator extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            languageFrom: "russian",
            languageTo: "english",
            inputText: "",
            outputText: "",
            submit: false,
        };
    }
 

    timeout = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    translate = (text,languageFrom,languageTo) => {
          var url="api/v1/translate/"+languageFrom+"/"+languageTo;
          console.log("translate");
          console.log(text);
          console.log("url="+url);

          /*
          return fetch(url, {
              method: 'POST', // *GET, POST, PUT, DELETE, etc.
              mode: 'cors', // no-cors, cors, *same-origin
              cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
              credentials: 'same-origin', // include, *same-origin, omit
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded',
              },
              redirect: 'follow', // manual, *follow, error
              referrer: 'no-referrer', // no-referrer, *client
              body: text
          })
          .then(response => {response.body});
        */
       this.timeout(2000).then (r=>{
            this.setState({outputText: "Dummy ответ от "+url+"\n POST="+text,submit: false});
        });
      }

    handleTranslate = () => {
        console.log("handleTranslate");
        console.log(this.state.inputText+","+this.state.languageFrom+","+this.state.languageTo);
        this.setState({submit: true,outputText: "Подождите..."});
        this.translate(this.state.inputText,this.state.languageFrom,this.state.languageTo);
    }

    render() {
        return (
            <Card>
                <CardHeader title="Переводчик" />
                <CardContent>
                    <Container>
                        <Row>
                            <Col>
                                <Form>
                                <Form.Group controlId="с1">
                                    <Form.Label>Оригинал</Form.Label>
                                    <Form.Control as="select" value={this.state.languageFrom} onChange={e=>{this.setState({languageFrom: e.target.value},this.handleTranslate);}}>
                                    {Object.keys(Languages).map(key => (
                                                            <option value={key}>{Languages[key]}</option>
                                                        ))
                                    }
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group controlId="">
                                    <Form.Control as="textarea" rows="3"
                                        onChange={e=>{this.setState({inputText: e.target.value},this.handleTranslate);}}
                                        value={this.state.inputText} placeholder="Введите текст"/>
                                </Form.Group>
                                </Form>
                            </Col>
                            <Col>
                                <Form>
                                <Form.Group controlId="с2">
                                    <Form.Label>Перевод</Form.Label>
                                    <Form.Control as="select" value={this.state.languageTo} onChange={e=>{this.setState({languageTo: e.target.value},this.handleTranslate);}}>
                                    {Object.keys(Languages).map(key => (
                                                            <option value={key}>{Languages[key]}</option>
                                                        ))
                                    }
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group controlId="">
                                    <Form.Control as="textarea" rows="3"
                                        value={this.state.outputText } placeholder="Перевод"/>
                                </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                </CardContent>

            </Card>
        );
    }
}

