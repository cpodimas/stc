import React from 'react';
import { Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import SpeechIcon from '@material-ui/icons/SettingsVoice';
import TranslateIcon from '@material-ui/icons/Translate';

import Construction from './routes/Construction';
import Translation from  './routes/Translation';


const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');
const App = () => (
    <Admin dashboard={Construction} dataProvider={dataProvider}>
        <Resource name="speech" list={Construction}  options={{ label: 'Перевод речи в текст' }}  icon={SpeechIcon}/>
        <Resource name="translation" list={Translation}  options={{ label: 'Переводчик' }}  icon={TranslateIcon}/>

    </Admin>
);



//export default Translation;
export default App;




